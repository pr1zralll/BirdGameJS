var ctx, w, h, objects = [];
$(document).ready(function () {
    init();
    setInterval(update, 15);
});
var bird;

function init() {
    var canvas = document.getElementById("main");
    w = $(window).width();
    h = $(window).height();
    canvas.width = w;
    canvas.height = h;
    ctx = canvas.getContext("2d");
    ctx.font = "20px Arial";
    ctx.fillStyle = "#7caeff";
    canvas.addEventListener("click", function (e) {
        var rect = canvas.getBoundingClientRect();
        var x = e.clientX - rect.left;
        var y = e.clientY - rect.top;
        click(x, y);
    });

    //init objects

    var imageBird = new Image(100 * 1.077, 100);
    imageBird.src = "img/bird.png"

    bird = new Sprite(400, 200, imageBird);

    objects.push(bird);
}

function click(x, y) {
    bird.jump();
}

function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

class Sprite {
    constructor(x, y, image) {
        this.x = x;
        this.y = y;
        this.dx = 0;
        this.dy = 0;
        this.width = image.width;
        this.height = image.height;
        this.image = image;
        this.m = 0.5;
    }
    draw() {
        ctx.beginPath();
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
        ctx.fill();
    }
    update() {
        if (this.dy > 0) {
            this.y -= this.m * this.dy;
            this.dy--;
        }
        if (this.y < 500) {
            this.y += this.m * 5;
        }
    }
    jump() {
        if (this.dy < 1) {
            this.dy = 30;
        }
    }
}

function update() {
    //clear
    ctx.clearRect(0, 0, w, h);
    //check collision
    check();

    //draw objects
    objects.forEach(function (item, index, array) {
        item.update();
        item.draw();
    });

    //fps
    ctx.fillText("fps: " + getFps(), 0, 30);
}

function check() {
    objects.forEach(function (item1, index1, array1) {
        objects.forEach(function (item2, index2, array2) {
            if (index1 != index2) {
                //todo
            }
        });
    });
}

var lastLoop = new Date;

function getFps() {
    var thisLoop = new Date;
    var fps = 1000 / (thisLoop - lastLoop);
    lastLoop = thisLoop;
    return Math.round(fps);
}
